
customElements.define('nav-bar', class extends HTMLElement {
  constructor() {
    super();
    this.search = false;
    this.searchIcon = false;
  }
  connectedCallback() {
    let arr = [];
    new Promise((resolve, reject) => {
      let items = [
        { name: 'Home' },
        // { name: 'About Us' },
        // { name: 'Contacts' }
      ];
      resolve(arr = items)
    });
    const shadow = this.attachShadow({ mode: 'open' });

    let listItem = ``;
    for (let i = 0; i < arr.length; i++) {
      listItem += `
          <nav-items data=${JSON.stringify(arr[i])}></nav-items>
          `
    };

    shadow.innerHTML = `
        <style>
          @import "./components/nav-bar/nav-bar.css";
        </style>
        <div>
          <a href="#">
            <img src="../assets/logo.png" width="50" height="50" alt="logo">
          </a>
        </div>
        <ul>
          ${listItem}
        </ul>
        ${this.searchDiv}
      `;
    const list = document.querySelector('nav-items');
    console.log(list);
  };

  get searchDiv() {
    if (this.hasAttribute('search')) {
      return `
      <div class='search'>
      <form role="search" method="get" class="search-form form">
      <label>
            <span class="screen-reader-text">Search for...</span>
          <input type="search" class="search-field" placeholder="Type something..." value="" name="s" title="" />
      </label>
      <input type="submit" class="search-submit button" value="${this.searchIcon ? '&#xf002' : 'GO'}" />
      </form>
    </div>
      `
    } else {
      return ''
    }
  }
  static get observedAttributes() {
    return ['search', 'search-icon'];
  }
  attributeChangedCallback(name, oldValue, newValue) {
    name === 'search-icon' ? this.searchIcon = true : this.searchIcon = false;
  }
});