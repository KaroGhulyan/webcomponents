customElements.define('nav-search', class extends HTMLElement {
    connectedCallback() {
        const shadow = this.attachShadow({ mode: 'open' });
        shadow.innerHTML = `
        <style>
            @import "./components/nav-search/nav-search.css";
        </style>
        <form role="search" method="get" class="search-form form">
        <label>
            <span class="screen-reader-text">Search for...</span>
             <input type="search" class="search-field" placeholder="Type something..." value="" name="s" title="" />
            </label>
             <input type="submit" class="search-submit button" value="&#xf002" />
        </form>
      `;
    };
    static get observedAttributes() {
        return ['hide'];
    }
    attributeChangedCallback(name, oldValue, newValue) {
        name === 'hide' ? this.classList.add('d-none') : null
    }

});