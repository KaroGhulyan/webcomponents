customElements.define('nav-items', class extends HTMLElement {
    connectedCallback() {
        console.log(this.getAttribute('data'))
        let arr = JSON.parse(this.getAttribute('data'));
        console.log(arr)
        const shadow = this.attachShadow({ mode: 'open' });
        shadow.innerHTML = `
        <style>
            @import "./components/nav-item/nav-item.css";
        </style>
          <li>
          <a href="#">
              ${arr.name}
          </a>
         </li>
      `;
    }
});


